FROM openjdk:10
VOLUME /tmp
ADD /build/libs/*.jar apptwo.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/apptwo.jar"]
