package ee.seos.seos.usecases.searchbyword;


import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

import static ee.seos.seos.SeosApplication.emtakDB;

@Component
public class SearchByEmtak {

  public List<Object> execute(String query){
    return emtakDB.get(query);
  }
}
