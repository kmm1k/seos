package ee.seos.seos;

import ee.seos.seos.emtak.generated.Claset;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.ClassPathResource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class SeosApplication {

	//public static Claset dataSet;
	public static HashMap<String, List<Object>> emtakDB;

	public static void main(String[] args) throws JAXBException, IOException {
		/*JAXBContext context = JAXBContext.newInstance(Claset.class);
		dataSet = (Claset) context.createUnmarshaller()
				.unmarshal(new ClassPathResource("classpath:db.xml").getFile());*/

		InputStream is = new ClassPathResource("json/7911.json").getInputStream();
		InputStream is2 = new ClassPathResource("json/7912.json").getInputStream();



		emtakDB = new HashMap();
		emtakDB.put("7911", Collections.singletonList(new ObjectMapper().readValue(stream2file(is), Object.class)));
		emtakDB.put("7912", Collections.singletonList(new ObjectMapper().readValue(stream2file(is2), Object.class)));
		emtakDB.put("791", List.of(emtakDB.get("7911").get(0), emtakDB.get("7912").get(0)));

		SpringApplication.run(SeosApplication.class, args);
	}

	public static final String PREFIX = "json";
	public static final String SUFFIX = ".json";

	public static File stream2file (InputStream in) throws IOException {
		final File tempFile = File.createTempFile(PREFIX, SUFFIX);
		tempFile.deleteOnExit();
		try (FileOutputStream out = new FileOutputStream(tempFile)) {
			IOUtils.copy(in, out);
		}
		return tempFile;
	}


}
