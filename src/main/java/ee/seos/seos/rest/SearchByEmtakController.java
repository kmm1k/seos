package ee.seos.seos.rest;

import ee.seos.seos.usecases.searchbyword.SearchByEmtak;
import ee.seos.seos.usecases.searchbyword.SearchByWord;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class SearchByEmtakController {
  private final SearchByEmtak searchByEmtak;

  public SearchByEmtakController(SearchByEmtak searchByEmtak) {
    this.searchByEmtak = searchByEmtak;
  }

  @GetMapping("/emtak/{query}")
  List<Object> getServer(@PathVariable String query) throws IOException {
    return searchByEmtak.execute(query);
  }

}
