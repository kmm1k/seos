package ee.seos.seos.rest;

import ee.seos.seos.usecases.searchbyword.SearchByWord;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.function.ServerResponse;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class SearchByWordController {
  private final SearchByWord searchByWord;

  public SearchByWordController(SearchByWord searchByWord) {
    this.searchByWord = searchByWord;
  }

  @GetMapping("/search/{query}")
  List<String> getServer(@PathVariable String query) throws IOException {
    return searchByWord.execute(query);
  }

}
